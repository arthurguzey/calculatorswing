package calculator.java.com;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class Panel extends JPanel {
    public double result=0;
    private JTextField numberOne = new JFormattedTextField("2");
    private JTextField numberTwo = new JFormattedTextField("2");
    private JTextField operation = new JFormattedTextField("+");
    private JTextField resultatText = new JFormattedTextField("4");
    public Panel(){

        setLayout(null);
        JButton resultButton = new JButton("Расчитать");
        resultButton.setBounds(5,155,270,50);
        add(resultButton);

        JLabel numberOneLabelText = new JLabel("Число 1");
        JLabel numberTwoLabelText = new JLabel("Число 2");
        JLabel operationLabelText = new JLabel("Операция");
        JLabel resultLabelText = new JLabel("Ответ");


        numberOneLabelText.setBounds(5,5,65, 50);
        add(numberOneLabelText);

        numberTwoLabelText.setBounds(5,55,65, 50);
        add(numberTwoLabelText);


        operationLabelText.setBounds(5,105,65, 50);
        add(operationLabelText);

        resultLabelText.setBounds(5,205,65, 50);
        add(resultLabelText);


        operation.setBounds(75,105,200, 50);
        operation.setEditable(true);
        add(operation);


        resultatText.setBounds(75,205,200, 50);
        resultatText.setEditable(false);
        add(resultatText);


        numberOne.setBounds(75,5,200, 50);
        numberOne.setEditable(true);
        add(numberOne);

        numberTwo.setBounds(75,55,200, 50);
        numberTwo.setEditable(true);
        add(numberTwo);



        ActionListener listenerResult = (ActionEvent e) ->
        {
            double a = Double.parseDouble(numberOne.getText());
            double b = Double.parseDouble(numberTwo.getText());
            String operationReader = operation.getText();
            switch (operationReader){
                case ("+"):
                    result = a+b;
                    break;
                case("*"):
                    result = a*b;
                    break;
                case("-"):
                    result = a-b;
                    break;
                case("/"):
                    result = a/b;
                default:
                    resultatText.setText("Error");
                    break;
            }
            resultatText.setText(String.valueOf(result));
        };
        resultButton.addActionListener(listenerResult);

        /*addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                char symbol = e.getKeyChar();
                if(!Character.isDigit(symbol))
                {
                    return;
                }
                else
                {
                    numberOne.setText(numberOne.getText()+symbol);
                    numberTwo.setText(numberTwo.getText()+symbol);
                }

            }
        });

         */
    }
}
